# Serveur de traitement d'image

Notre serveur permet d'effectuer des opérations de traitement et de manipulation d'image que l'on peut trouver dans des logiciels de retouche photo. 

  
### Version
1.0

### Technologies

Notre serveur d'image utilise un nombre de technologie open source afin de fonctionner correctement :
* [C] - Traitement de l'image
* [PHP5] -  [Javascript] - [jQuery] - Développement des fonctionnalités du serveur
* [HTML5] -[CSS3] - [Bootstrap] -  Mise en forme du serveur et gestion de l'affichage des images


### Installation
Vous avez besoin d'avoir un [lamp] installé. 

Vous devez installer le plugin [Imagick] pour PHP5. 
```sh
$ sudo apt-get install php5-imagick
```
Puis redémarrer apache2
```sh
$ sudo service apache2 restart
```

Vous devez mettre le dossier du projet dans le dossier localhost de votre lamp (e.g. /var/www/html/dossier_projet/).

Attribuez les droits au dossier du projet avec la commande suivante :
```sh
$ sudo chmod -R 757 dossier_projet/
```
Vous pouvez accèder au projet avec n'importe quel navigateur en tapant dans l'url : localhost/dossier_projet/Web/menu.html

### Attention
Si vous souhaitez uploader des images au format ppm de grande taille, vous devrez modifier votre fichier php.ini en modifiant la partie suivante :
```sh
; Maximum allowed size for uploaded files.
; http://php.net/upload-max-filesize
upload_max_filesize = 100M 
```
Ici, nous avons fixé la taille d'upload maximun à 100M mais vous êtes libre de mettre la valeur qui vous convient. Une fois la valeur modifiée n'oubliez pas de redémarrer apache2 (en utilisant la commande vue ci-dessus).


### Fonctionnalités
#### Fichier 
* Ouvrir : vous pouvez selectionner une image au format ppm parmi les fichiers de votre ordinateur
* Recharger : après que vous ayez effectué des modification sur l'image de base vous pouvez choisir de la réinitialiser
* Enregistrer : une fois les modifications effectuées vous pouvez choisir de télécharger l'image au format ppm qui se situera dans votre dossier de téléchargement par défaut
* Quitter : 
#### Elémentaire
* Binarisation par seuillage
* Négatif
* Conversion en niveaux de gris
* Amélioration du contraste
* Symétrie horizontale
* Symétrie verticale
* Redimensionnement
##### Convolution 
* Lissage
* Grandient simple
* Grandient avec l'opérateur de Sobel
* Laplacien
#### Avancés 
* Détection de contours avec l'opérateur de Sobel
* Détection de contours Laplacien
* Débruitage : filtre médian



**Amusez vous bien !**

[//]: # (These are reference links used in the body of this note and get stripped out when the markdown processor does its job. There is no need to format nicely because it shouldn't be seen. Thanks SO - http://stackoverflow.com/questions/4823468/store-comments-in-markdown-syntax)


   [C]: <https://fr.wikipedia.org/wiki/C_(langage)>
   [HTML5]: <https://fr.wikipedia.org/wiki/HTML5>
   [PHP5]: <https://fr.wikipedia.org/wiki/PHP>
   [Javascript]: <https://fr.wikipedia.org/wiki/JavaScript>
   [CSS3]: <https://fr.wikipedia.org/wiki/Feuilles_de_style_en_cascade>
   [Bootstrap]: <http://twitter.github.com/bootstrap/>
   [jQuery]: <http://jquery.com>
   
   [lamp]: <https://doc.ubuntu-fr.org/lamp>
   [Imagick]: <http://php.net/manual/fr/class.imagick.php>

   

