<?php
include 'utils.php';

$uploaddir = 'uploads/';

if (isset($_FILES['pictureFile'])) {
    //Vérifie la taille de l'image
    if ($_FILES["pictureFile"]["size"] > 10000000) {
        header($_SERVER["SERVER_PROTOCOL"] . ' 400 Bad Request');
        $response = 'La taille de fichier est limitée à 10MB';
        die($response);
    }

    //Création du dossier si existe pas
    if (!file_exists($uploaddir))
        mkdir($uploaddir, 0777, true);

    $filename = explode('.', basename($_FILES['pictureFile']['name']))[0] . rand(1, PHP_INT_MAX) . '.ppm';

    //Déplace le fichier temporaire dans le dossier destination et convertie le fichier
    if (move_uploaded_file($_FILES['pictureFile']['tmp_name'], $uploaddir . $filename))
        echo json_encode(getImage(__DIR__ . '/' . $uploaddir, $filename));
    else {
        header($_SERVER["SERVER_PROTOCOL"] . ' 500 Internal Server Error');
        echo 'Image non convertie';
    }
} else {
    header($_SERVER["SERVER_PROTOCOL"] . ' 500 Internal Server Error');
    echo "Problème d'upload";
}
