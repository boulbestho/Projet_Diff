$(document).ready(function () {
    var canvas;  //canvas
    var ctx;    //context

    var $container = $('#zone');
    var $selection = $('<div>').addClass('selection-box');

    var debut = {x: 0, y: 0};
    var fin = {x: 0, y: 0};

    var history = [];   //undo
    var reload; //Dernière image non modifiée

    //Prototype pour obtenir le dernier élement d'un tab
    if (!Array.prototype.last) {
        Array.prototype.last = function () {
            return this[this.length - 1];
        };
    }
    ;

    $('li').not('.always-enabled').addClass('disabled');    //Permet de griser les liens
    $('.alert').removeClass("alert-error alert-success alert-warning alert-info").hide();

    $('.alert .close').on('click', function () {    //Si l'utilisateur clique sur la croix pour enlever l'alert
        $(this).parent().hide('slow');
    });

    var displayError = function (jqXHR, textStatus, errorThrown) {  //Fonction qui affiche la dernière erreur sous forme d'alert
        $('.wrapperloading').css('display', 'none');
        $('.message').html(textStatus + ' : ' + errorThrown + '<br />' + jqXHR.responseText);
        $('.alert').removeClass("alert-error alert-success alert-danger alert-warning alert-info").addClass('alert-danger');
        $('.alert').show('slow');
        $(".alert").fadeTo(3000, 500).slideUp(500, function () {
            $(this).hide();
        });
    };

    //Fonction dessiner image
    var drawImage = function (id, img) {
        var image = (id === 'reload') ? reload : history.last();    //Suivant si c'est un reload on charge l'img pour régler l'affichage (crop ou img normale)
        var prop = $('#zone').width() / image.width;    //Proportion de l'image

        $('#zone').css('height', image.height * prop);  //Définie la hauteur en fonction de la largeur auto

        canvas = $('#canvas').get(0);
        ctx = canvas.getContext('2d');

        $(img).load(function () {   //Une fois l'image chargée
            var h = $('#zone').height();
            var w = $('#zone').width();

            $('li').not('.always-enabled').removeClass('disabled'); //Permet à l'utilisateur d'effectuer des modifs sur l'image

            ctx.canvas.height = h;
            ctx.canvas.width = w;
            ctx.drawImage(this, 0, 0, w, h);

            $('.wrapperloading').css('display', 'none');    //Enleve le loader

            $('#crop').addClass('disabled');    //Empeche le crop si aucune selection
            $('#zone').css('display', 'block'); //Affiche l'img

            $('.message').text('Image chargée avec succès!');

            $('.alert').removeClass("alert-error alert-success alert-danger alert-warning alert-info").addClass('alert-success');
            $('.alert').show('slow');
            $(".alert").fadeTo(1500, 500).slideUp(500, function () {
                $(this).hide();
            });
        });
    };

    //A la fin de la session, suppression des images du serv
    $('#exit').on('click', function () {
        var jqXH = $.ajax({
            url: 'delete.php',
            success: function () {
                $('#coords').text('Aucune image ouverte.');
                $('#colors').empty();
                $('#zone').css('display', 'none');
                $('.message').text('Session terminée avec succès!');

                $('.alert').removeClass("alert-error alert-success alert-danger alert-warning alert-info").addClass('alert-success');
                $('.alert').show('slow');
                $(".alert").fadeTo(1500, 500).slideUp(500, function () {
                    $(this).hide();
                });
            }
        });

        jqXH.fail(function (jqXHR, textStatus, errorThrown) {
            displayError(jqXHR, textStatus, errorThrown);
        });

        $('li').not('.always-enabled').addClass('disabled');    //Empêche l'utilisateur d'effectuer des opérations non autorisées
        $('#input_upload').val(''); //A cause du on change de l'input file
    });


    $(".img-mod-data").click(function () {
        $("#func_id").val($(this).data('id'));  //Permet de stocker l'id de la fonction dans un input caché
    });

    //Appel ajax pour exec en php le prog c
    $('.img-mod, #exec').on('click', function (e) {
            var id;
            var val = $('#seuil_value').val();

            if ($(this).parent().hasClass('disabled'))   return;

            id = ($(this).attr('id') === 'exec') ? $("#func_id").val() : e.target.id;   //Distinction entre les nodes

            if (val < 0 || val > 255) { //Verif le seuil
                $('.input-group').addClass('has-error');
                return false;
            }

            $('.input-group').removeClass('has-error'); //Remet le style par défaut

            if (ctx) {  //Si il y a une image
                $('.wrapperloading').css('display', 'block');   //Fait apparaitre le loading

                var jqXH = $.post("exec.php", { //POST les valeurs.
                    id: id,
                    original: history.last().original,  //Dernière image ppm
                    binarisation: val,
                    debut: debut,   //Coord du crop (debut)
                    fin: fin    //Coord du crop (fin)
                }, function (data) {    //Suite à un echo du php
                    var $img = $('<img>', {src: data.src}); //Création d'une image
                    var last = {src: data.src, original: data.original, width: data.width, height: data.height};

                    history.push(last); //Ajoute la dernière image reçue à l'historique
                    drawImage(id, $img);    //Déssine l'image
                    $selection.remove();    //Enlève le cadre rouge du crop
                }, "json");

                jqXH.fail(function (jqXHR, textStatus, errorThrown) {   //Si le post rencontre une erreur
                    $('.wrapperloading').css('display', 'none');    //Fait disparaitre le loading
                    $('.message').html(textStatus + ' : ' + errorThrown + '<br />' + jqXHR.responseText);
                    $('.alert').removeClass("alert-error alert-success alert-danger alert-warning alert-info").addClass('alert-danger');
                    $('.alert').show('slow');
                    $(".alert").fadeTo(3000, 500).slideUp(500, function () {
                        $(this).hide();
                    });
                });
            }
        }
    );

    //Gère l'affichage des infos (code RGB, coordonnées)
    $('#canvas').mousemove(function (e) {
        if (ctx) {
            var position = $('#zone').position();   //Recup la position de la zone
            var x = e.pageX - parseInt(position.left);  //S'assure que la valeur est un entier et aligne la base sur le coin gauche supérieur de l'img
            var y = e.pageY - parseInt(position.top);
            var data = ctx.getImageData(x, y, 1, 1);    //Pour la couleur

            $('#coords').text('Coordonnées : ' + '(' + x + ', ' + y + ')');
            $('#colors').text('Couleurs : (R: ' + data.data[0] + ' G: ' + data.data[1] + ' B: ' + data.data[2] + ')');
        }
    });

    //CROP
    $('#zone').mousedown(function (e) {
        var position = $('#zone').position();
        var start_x = e.pageX - parseInt(position.left);    //Coord en x relative du debut de la selection
        var start_y = e.pageY - parseInt(position.top);
        var newdeb_x = 0, newdeb_y = 0;
        var newfin_x = 0, newfin_y = 0;

        $selection.css({    //Debut du cadre rouge en css
            'top': start_y,
            'left': start_x,
            'width': 0,
            'height': 0
        });

        $selection.appendTo($container);    //Ajoute un calque sur l'image. Ce calque est transparent avec les bords rouge

        $container.mousemove(function (e) {
            var move_x = e.pageX - parseInt(position.left); //Dernière valeur relative en x
            var move_y = e.pageY - parseInt(position.top);
            var w = Math.abs(move_x - start_x); //Longueur
            var h = Math.abs(move_y - start_y); //Hauteur
            var prop = $('#zone').width() / history.last().width;   //Calcul de la proportion de l'img ex: 4/3, 16/9, etc...

            /*
             *Permet d'effectuer le crop dans toute direction
             */
            newdeb_x = (move_x < start_x) ? (start_x - w) : start_x;
            newdeb_y = (move_y < start_y) ? (start_y - h) : start_y;

            newfin_x = (move_x < start_x) ? (move_x + w) : move_x;
            newfin_y = (move_y < start_y) ? (move_y + h) : move_y;

            $selection.css({    //Fin du cadre rouge en css
                'width': w,
                'height': h,
                'top': newdeb_y,
                'left': newdeb_x
            });

            debut = {x: parseInt(newdeb_x / prop), y: parseInt(newdeb_y / prop)};   //Structure utilisée dans le prog c qui défini la coord du pt de départ
            fin = {x: parseInt(newfin_x / prop), y: parseInt(newfin_y / prop)};
        }).on('mouseup', function (e) { //Une fois le bouton de la souris relaché
            $container.off('mousemove');    //Détache l'évenement
            $('#crop').removeClass('disabled');

            if ($selection.css('width') == '4px' && $selection.css('height') == '4px')  //Fix le point rouge
                $selection.remove();

            if (!newfin_x)    $('#crop').addClass('disabled');  //Empêche le crop si c'est un point
        });
    });

    //Modifie les dimensions lors d'un resize de la fenêtre
    $(window).resize(function () {
        if (ctx) {
            var $img = $('#image').get(0);
            drawImage('', $img);
        }
    });

    $('#link_upload').on('click', function (e) {
        e.preventDefault(); //Empêche le comportement par defaut
        $('#input_upload:hidden').trigger('click'); //Simul un click
    });

    //Empêche les clicks si aucune image chargée
    $('a.dropdown-toggle, a, a#redimensionnement').click(function (e) {
        if ($(this).parent().hasClass('disabled')) {
            e.stopImmediatePropagation();   //Très important!! sinon click quand même
            e.preventDefault();
            return;
        }
    });

    //Au click, modifie les attributs href et download du lien
    $('#save_as').on('click', function () {
        var url = window.location.protocol + '//' + window.location.hostname + '/Projet_Diff/Web/' + history.last().original;

        //Modif des attributs
        $(this).find("a").attr('download', 'download.ppm');
        $(this).find("a").attr('href', url);
    });

    //Affiche la dernière image uploadée (reload)
    $('#reload').on('click', function () {
        drawImage('reload', $('<img>', {src: reload.src}));
    });

    //Annule la dernière modif
    $('#undo').on('click', function () {
        if (history.length > 1) {   //Suppr pas la première image
            history.pop();  //Suppr dernière modif de l'historique
            drawImage('undo', $('<img>', {src: history.last().src}));
        } else {    //Affiche message
            $('.message').text('Aucune modification à annuler');
            $('.alert').removeClass("alert-error alert-success alert-danger alert-warning alert-info").addClass('alert-info');
            $('.alert').show('slow');
            $(".alert").fadeTo(1500, 500).slideUp(500, function () {
                $(this).hide();
            });
        }
    });

    $('#input_upload').on('change', function (e) {
        var file = e.target.files[0];
        var formData = new FormData();

        //Vérifie la taille du fichier et affiche un message à l'utilisateur en cas d'erreur
        if (file.size > 10000000) { //10 MB
            $('.message').text("La taille de fichier est limitée à 10MB!");
            $('.alert').removeClass("alert-error alert-success alert-danger alert-warning alert-info").addClass('alert-warning');
            $('.alert').show('slow');
            $(".alert").fadeTo(3000, 500).slideUp(500, function () {
                $(this).hide();
            });
            return;
        }

        //Vérifie le type du fichier et affiche un message à l'utilisateur en cas d'erreur
        if (!file.type.match(/image\/x-portable-pixmap/)) {
            $('.message').text("Formats de fichier acceptés: .ppm");
            $('.alert').removeClass("alert-error alert-success alert-danger alert-warning alert-info").addClass('alert-warning');
            $('.alert').show('slow');
            $(".alert").fadeTo(3000, 500).slideUp(500, function () {
                $(this).hide()
            });
            return;
        }

        formData.append('pictureFile', file);   //JSON

        var jqXH = $.ajax({
            url: 'upload.php',
            type: 'POST',
            processData: false,
            contentType: false,
            data: formData,
            dataType: 'json',
            beforeSend: function () {   //Avant l'envoi, on affiche le loading
                $('.wrapperloading').css('display', 'block');
            },
            success: function (response) {  //Si le post est ok
                var $img = $('<img>', {src: response.src}); //Création de l'image
                var last = {    //Infos img
                    src: response.src,
                    original: response.original,
                    width: response.width,
                    height: response.height
                };

                $('#coords').text('Bougez le curseur de la souris sur l\'image pour voir apparaître les informations en temps réel.');

                history.push(last);
                reload = {src: response.src, width: response.width, height: response.height};  //Dernière image uploadée. Simplifie le reload.
                drawImage('reload', $img);
            }
        });

        jqXH.fail(function (jqXHR, textStatus, errorThrown) {   //En cas d'erreur
            displayError(jqXHR, textStatus, errorThrown);
        });
    });
});

