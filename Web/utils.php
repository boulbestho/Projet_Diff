<?php
session_start();

if (!isset($_SESSION['files']))
    $_SESSION['files'] = array();

/**
 * Converti une image ppm en image png.
 *
 * @param $path
 * @param $filename
 * @return array
 */
function getImage($path, $filename)
{
    $image = new Imagick($path . $filename);

    $image->setImageFormat("png");
    $new_filename = explode('.', $filename)[0] . '.png';
    $image->writeImage($path . $new_filename);
    $width = $image->getImageWidth();
    $height = $image->getImageHeight();

    $_SESSION['files'][] = $path . $filename;
    $_SESSION['files'][] = $path . $new_filename;

    return array('width' => $width, 'height' => $height, 'src' => 'uploads/' . $new_filename, 'original' => 'uploads/' . $filename);
}