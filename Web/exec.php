<?php
include 'utils.php';

$cmd = array(
    'binarisation' => 1,
    'negatif' => 2,
    'gris' => 3,
    'contraste' => 4,
    'symetrie_h' => 5,
    'symetrie_v' => 6,
    'redimensionnement' => 7,
    'lissage' => 8,
    'grad-simple' => 9,
    'grad-sobel' => 10,
    'laplacien' => 11,
    'contour-sobel' => 12,
    'contour-laplacien' => 13,
    'debruitage' => 14,
);

if (isset($_POST['id']) && isset($_POST['original']) && isset($_POST['binarisation']) && isset($_POST['debut']) && isset($_POST['fin'])) {
    $id = $_POST['id'];
    $bin = $_POST['binarisation'];
    $debut = $_POST['debut'];
    $fin = $_POST['fin'];
    $original = getcwd() . '/' . $_POST['original'];
    $output = array();
    $return_var = -1;

    $command = './bin/projet ' .
        escapeshellarg($cmd[$id]) . /*commande*/
        ' ' .
        escapeshellarg($original) . /*chemin*/
        ' ' .
        escapeshellarg($bin) .  /*Seuil binarisation*/
        ' ' .
        escapeshellarg($debut['x']) .   /*valeur crop debut x*/
        ' ' .
        escapeshellarg($debut['y']) .
        ' ' .
        escapeshellarg($fin['x']) .
        ' ' .
        escapeshellarg($fin['y']);

    if (chdir('../Code_C/')) {
        exec($command, $output, $return_var);

        $result = array_merge(getImage(__DIR__ . '/uploads/', $output[0]), array('return' => $return_var));

        echo json_encode($result);
    }
} else
    echo json_encode(array('error' => 'Avez-vous bien ouvert une image?'));