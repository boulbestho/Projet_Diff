<?php
session_start();

if (isset($_SESSION['files'])) {
    $files = $_SESSION['files'];

    foreach ($files as $file)
        unlink($file);

} else {
    header($_SERVER["SERVER_PROTOCOL"] . ' 500 Internal Server Error');
    echo "Images non supprimées";
}