#Installation

- Décompresser l'archive avec la commande:
$tar -zxvf chemin_fichier/nomdufichier && cd chemin_fichier/Projet_Diff/

#Compilation

- Taper la commande suivante:
$make

#Netoyage

- Taper la commande suivante:
$make clean

#Utilisation

- À partir de la console:
$./bin/projet

#Images

- Les images modifiées sont enregistrées dans ./img

#Doxygen

- Pour ouvrir la page d'accueil du Doxygen : ouvrir ./html/index.html



