/**
 * \file fonctionnalités_avancées.c
 * \brief Contient les fonctionnalitées avancées du projet.
 * \author Thomas BOULBES, Ivan HANZEK, Romain PAPOT, Maxime TAUZIA
 * \version 0.1
 * \date 10 septembre 2015 au 20 octobre 2015
 *
 *
 *
 */
#include "fonctionnalités_basiques.h"
#include "fonctionnalités_avancées.h"
#include "fonctions_annexes.h"
#include "fonctions_gestion_image.h"


/**
 * \fn void Filtre_Median(Image* I) 
 * \author Thomas BOULBES, Ivan HANZEK, Romain PAPOT, Maxime TAUZIA
 * \date 7 octobre 2015
 * \brief Fonction qui applique le filtre median sur une image.
 * \param I -> Image
 */
void Filtre_Median(Image* I) 
{
	int i, j;
	Image* I_tmp = Ajout_Bordure(I, -1); //Ajout de bordures de valeurs -1
	for (i = 1; i < I->r+1; i++)
	{
		for (j = 1; j < I->c+1; j++)
		{
			I->Tab[i-1][j-1] = Applique_Luminosite(Mediane(I_tmp, i, j)); // On applique les médianes sur le nouveau tableau. 
		}
	}
	DelImage(I_tmp); // On supprime l'image temporaire. 
}
/**
 * \fn void Applique_Filtre(int i, Image* I) 
 * \author Thomas BOULBES, Ivan HANZEK, Romain PAPOT, Maxime TAUZIA
 * \date 07/10/2015
 * \brief Fonction qui suivant une choix applique un filtre median sur une image.
 * \param   i :  entier
 * \param	I -> Image
 */
void Applique_Filtre(int i, Image* I) 
{	
	Image* Lx = NouvelleImage(I->r, I->c); 
	Image* Ly = NouvelleImage(I->r, I->c);
	Copie_Image(I, Lx);// Lx devient une copie de I. 
	Copie_Image(I, Ly);// Lx devient une copie de I. 
	switch (i) {
		case 1 : 			
			Convolution(I,Filtre_1);
			Contraste(I);
		break;

		case 2 :
			Convolution(Lx, Filtre_2);
			Convolution(Ly, Filtre_3);
			Sobel(I, Lx, Ly, &Sobel_sqrt);
			Contraste(I);
		break;

		case 3 : 
			Convolution(Lx, Filtre_4);
			Convolution(Ly, Filtre_5);
			Sobel(I, Lx, Ly, &Sobel_abs);
			Contraste(I);
		break;

		case 4 :
			Convolution(I,Filtre_6);
			Contraste(I);
		break;			
	}
	DelImage(Lx);
	DelImage(Ly);
}


/**
 * \fn void Contours_Sobel(Image* I) 
 * \author Thomas BOULBES, Ivan HANZEK, Romain PAPOT, Maxime TAUZIA
 * \date 7 octobre 2015
 * \brief Fonction qui met en évidence les contours d'une image mise en paramètre suivant Sobel.  
 * \param  I -> Image
 */
void Contours_Sobel(Image* I, int s) 
{
	Image* Lxy = NouvelleImage(I->r, I->c);
	Mat_GxGy(I, Lxy); // Remplissage de Lxy. 
	Troncature(Lxy); //troncature des valeurs de Lxy. 
	Binarisation(Lxy,s);//Binarisation de Lxy. 
	Copie_Image(Lxy, I); //I prend la valeur de Lxy. 
	DelImage(Lxy); //Suppression de Lxy. 
}

/**
 * \fn Contours_Laplacien(Image* I) 
 * \author Thomas BOULBES, Ivan HANZEK, Romain PAPOT, Maxime TAUZIA
 * \date 8 octobre 2015
 * \brief Fonction qui met en évidence les contours d'une image mise en paramètre suivant le Laplacien.
 * \param	I -> Image
 */
void Contours_Laplacien(Image* I,int s) 
{
	Image* Lxy = NouvelleImage(I->r, I->c);
	Image* Gxy = NouvelleImage(I->r, I->c);
	Parcourir(I, Niveau_De_Gris);
	Applique_Filtre(1,I);
	Copie_Image(I, Lxy);
	Copie_Image(I, Gxy);
	Applique_Filtre(4, Lxy);
	Applique_Filtre(2, Gxy);
	Compare_Laplacien(I, Gxy, Lxy);
	Binarisation(I,s);
	DelImage(Lxy);
	DelImage(Gxy);
}


