/**
 * \file fonctionnalités_basiques.c
 * \brief Contient les fonctionnalitées basiques du projet.
 * \author Thomas BOULBES, Ivan HANZEK, Romain PAPOT, Maxime TAUZIA
 * \version 0.1
 * \date 10 septembre 2015 au 20 octobre 2015
 *
 *
 *
 */
#include "fonctionnalités_basiques.h"
#include "fonctions_annexes.h"
#include "fonctions_gestion_image.h"


/**
 * \fn void Binarisation(Image* I)
 * \author Thomas BOULBES, Ivan HANZEK, Romain PAPOT, Maxime TAUZIA
 * \date 22 septembre 2015
 * \brief Modifie le tableau de pixels afin d'y appliquer la Binarisation.
 * \param	I -> Image
 */
void Binarisation(Image* I,int s) 
{
	int i, j;
	while (s < 0 || s > 255){
		printf("\nChoisissez un seuil de Binarisation : ");
		s = scan_int(s, "\nChoisissez un seuil de Binarisation : ");
	}
	for (i = 0; i < I->r; ++i)
		for (j = 0; j < I->c; ++j)
		{
			if (I->Tab[i][j].r >= s) I->Tab[i][j] = Applique_Luminosite(255);
			else I->Tab[i][j] = Applique_Luminosite(0);
		}
}


/**
 * \fn Pixel Negatif(Pixel p)
 * \author Thomas BOULBES, Ivan HANZEK, Romain PAPOT, Maxime TAUZIA
 * \date 22 septembre 2015
 * \brief Retourne le pixel en négatif.
 * \param   Pixel  : P (Pixel est une structure, voir librairie.h)			
 * \return retourne un Pixel (Pixel est une structure, librairie.h)
 */
Pixel Negatif(Pixel p) 
{
	p.r = 255 - p.r;
	p.g = 255 - p.g;
	p.b = 255 - p.b;
	return p;
}

/**
 * \fn Pixel Niveau_De_Gris(Pixel p)
 * \author Thomas BOULBES, Ivan HANZEK, Romain PAPOT, Maxime TAUZIA
 * \date 22 septembre 2015
 * \brief Retourne le pixel en niveau de gris et fonctionne avec la fonction "Parcourir".
 * \param   Pixel : P (Pixel est une structure, voir librairie.h)			
 * \return retourne un Pixel (Pixel est une structure, librairie.h)
 */
Pixel Niveau_De_Gris(Pixel p) 
{
	int l = 0.3*p.r + 0.59*p.g + 0.11*p.b;
	return Applique_Luminosite(l);
}

/**
 * \fn void Contraste(Image* I)
 * \author Thomas BOULBES, Ivan HANZEK, Romain PAPOT, Maxime TAUZIA
 * \date 22 septembre 2015
 * \brief Normalise chaque valeur du tableau. 
 * \param	I -> Image
 */
void Contraste(Image* I)
 {
	int i, j;
	Pixel max, min;
	max = I->Tab[0][0];
	min = I->Tab[0][0];
	Pixel* p_max = &max;
	Pixel* p_min = &min;
	Get_Max_Min(I, p_max, p_min);
	for (i = 0; i < I->r; ++i)
		for (j = 0; j < I->c; ++j)
		{
			I->Tab[i][j].r  = (255*I->Tab[i][j].r)/(p_max->r-p_min->r) - (255*p_min->r)/(p_max->r-p_min->r);
			I->Tab[i][j].g  = (255*I->Tab[i][j].g)/(p_max->g-p_min->g) - (255*p_min->g)/(p_max->g-p_min->g);
			I->Tab[i][j].b  = (255*I->Tab[i][j].b)/(p_max->b-p_min->b) - (255*p_min->b)/(p_max->b-p_min->b);
		}
}


/**
 * \fn void Symetrie_H(Image* I)
 * \author Thomas BOULBES, Ivan HANZEK, Romain PAPOT, Maxime TAUZIA
 * \date 22 septembre 2015
 * \brief Applique la symétrie horizontale sur l'image
 * \param  I -> Image
 */
void Symetrie_H(Image* I) {
	int i, j;
	for (i = 0; i < I->r/2; ++i)
		for (j = 0; j < I->c; ++j)
		{
			I->Tab[i][j].r = I->Tab[(I->r-1)-i][j].r;
			I->Tab[i][j].g = I->Tab[(I->r-1)-i][j].g;
			I->Tab[i][j].b = I->Tab[(I->r-1)-i][j].b;
		}
}


/**
 * \fn void Symetrie_V(Image* I)
 * \author Thomas BOULBES, Ivan HANZEK, Romain PAPOT, Maxime TAUZIA
 * \date 22 septembre 2015
 * \brief Applique la symétrie verticale sur l'image
 * \param	I -> Image
 */
void Symetrie_V(Image* I) {
	int i, j;
	for (i = 0; i < I->r; ++i)
		for (j = 0; j < I->c/2; ++j)
		{
			I->Tab[i][j].r = I->Tab[i][(I->c-1)-j].r;
			I->Tab[i][j].g = I->Tab[i][(I->c-1)-j].g;
			I->Tab[i][j].b = I->Tab[i][(I->c-1)-j].b;
		}
}


/**
 * \fn Image* Pre_Crop(Image* I, Pos* debut, Pos* fin)
 * \author Thomas BOULBES, Ivan HANZEK, Romain PAPOT, Maxime TAUZIA
 * \date 28 septembre 2015
 * \brief  Retourne la nouvelle image initialisée.
 * \param  debut -> Position du début de la sélection.
 * \param  fin -> Position de la fin de la sélection.
 * \param  I -> Image			
 * \return retourne une Image (Image est une structure, voir librairie.h)
 */
Image* Pre_Crop(Image* I, Pos* debut, Pos* fin) {
	//char* message_erreur = " ";
	//printf("debut i : %d ; fin i : %d ; debut j : %d ; fin j : %d\n", debut->i, fin->i, debut->j, fin->j );
	while (debut->i >= fin->i || debut->j >= fin->j || debut->i < 0 || debut->j < 0 || fin->i >= I->r || fin->j >= I->c ) {
		//printf("%s \n %s \n Entrez les valeurs du points de départ.\n", clear, message_erreur);
		//printf("Rentrez la valeur de la ligne : ");
		debut->i = scan_int(debut->i, "Rentrez la valeur de la ligne : ");
		//printf("Rentrez la valeur de la colonne : ");
		debut->j = scan_int(debut->j, "Rentrez la valeur de la colonne : ");
		//printf("\n\n Entrez les valeurs du points d'arrivé.\n");
		//printf("Rentre la valeur de la ligne : ");
		fin->i = scan_int(fin->i, "Rentre la valeur de la ligne : ");
		//printf("Rentre la valeur de la colonne : ");
		fin->j = scan_int(fin->j, "Rentre la valeur de la colonne : ");
		//if (debut->i >= fin->i || debut->j >= fin->j) message_erreur = "Entrez des valeurs valides.\n";
	}
	Image* new_I = NouvelleImage((fin->i-debut->i), (fin->j-debut->j));
	return new_I;
}

/**
 * \fn void Crop(Image* I, Image* new_I, Pos debut, Pos fin)
 * \author Thomas BOULBES, Ivan HANZEK, Romain PAPOT, Maxime TAUZIA
 * \date 28 septembre 2015
 * \brief Supprime l'ancienne image, et modifie la nouvelle image pour afficher le Crop
 * \param 	I -> image à Crop
 * \param 	new_I -> image Cropée
 * \param 	debut -> Position du début de la sélection
 * \param 	fin -> position de la fin de la sélection
 */
void Crop(Image* I, Image* new_I, Pos debut, Pos fin)
 {
	int i, j, k, l;
	for (i = (debut.i), k = 0; i < (fin.i); i++, k++)
	{
		for (j = (debut.j), l = 0; j < (fin.j); j++, l++)
		{
			new_I->Tab[k][l] = I->Tab[i][j];
		}
	}
}
