/**
 * \file fonctions_gestion_image.h
 * \author Thomas BOULBES, Ivan HANZEK, Romain PAPOT, Maxime TAUZIA
 * \version 0.1
 * \date 10 septembre 2015 au 20 octobre 2015
 *
 *
 *
 */
#ifndef _FONCTIONS_GESTION_IMAGE_H_
#define _FONCTIONS_GESTION_IMAGE_H_

#include "librairie.h"

Image* NouvelleImage(int r, int c);

void DelImage(Image* I);

int Sauver(Image* I, const char* fichier);

void Reecriture(char* fichier);

Image* Charger(char* fichier);

#endif