/**
 * \file fonctionnalités_avancées.h
 * \author Thomas BOULBES, Ivan HANZEK, Romain PAPOT, Maxime TAUZIA
 * \version 0.1
 * \date 10 septembre 2015 au 20 octobre 2015
 *
 *
 *
 */
#ifndef _FONCTIONNALITES_AVANCEES_H_
#define _FONCTIONNALITES_AVANCEES_H_

#include "librairie.h"

void Filtre_Median(Image* I);

void Applique_Filtre(int i, Image* I);

void Contours_Sobel(Image* I, int s);

void Contours_Laplacien(Image* I, int s);

#endif