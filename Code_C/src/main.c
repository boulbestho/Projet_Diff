/**
 * \file main.c
 * \author Thomas BOULBES, Ivan HANZEK, Romain PAPOT, Maxime TAUZIA
 * \version 0.1
 * \date 10 septembre 2015 au 20 octobre 2015
 *
 *
 *
 */


#include "menu.h"

 
/**
 * \fn int main ()
 * \brief Entrée du programme.
 *
 * \return EXIT_SUCCESS - Arrêt normal du programme.
 */
int main(int argc, char** argv)
{
  if(argc == 8){
    Pos debut;
    Pos fin;
    int choix = atoi(argv[1]);
    int binarisation = atoi(argv[3]);

    debut.j = atoi(argv[4]);
    debut.i = atoi(argv[5]);
    fin.j = atoi(argv[6]);
    fin.i = atoi(argv[7]);

    Menu(choix, argv[2], binarisation, debut, fin);
  }else
    printf("Nombre d'arguments incorrect\n\n");

  return EXIT_SUCCESS;
}


