/**
 * \file fonctionnalités_basiques.h
 * \author Thomas BOULBES, Ivan HANZEK, Romain PAPOT, Maxime TAUZIA
 * \version 0.1
 * \date 10 septembre 2015 au 20 octobre 2015
 *
 *
 *
 */
#ifndef _FONCTIONNALITES_BASIQUES_H_
#define _FONCTIONNALITES_BASIQUES_H_

#include "librairie.h"

void Binarisation(Image* I, int s);

Pixel Negatif(Pixel p);

Pixel Niveau_De_Gris(Pixel p);	

void Contraste(Image* I);

void Symetrie_H(Image* I);

void Symetrie_V(Image* I);

Image* Pre_Crop(Image* I, Pos* debut, Pos*  fin);

void Crop(Image* I, Image* new_I, Pos debut, Pos fin);

#endif