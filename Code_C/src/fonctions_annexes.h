/**
 * \file fonctions_annexes.h
 * \author Thomas BOULBES, Ivan HANZEK, Romain PAPOT, Maxime TAUZIA
 * \version 0.1
 * \date 10 septembre 2015 au 20 octobre 2015
 *
 *
 *
 */
#ifndef _FONCTIONS_ANNEXES_H_
#define _FONCTIONS_ANNEXES_H_

#include "librairie.h"

int scan_int(int choix, char* message);

Pixel Applique_Luminosite(int l);

Pos Init_Pos();

void Parcourir(Image* I, Pixel (*function)(Pixel));

void Get_Max_Min(Image* I, Pixel* p_max, Pixel* p_min);

int Compte_Valeurs(Image* I, int p_i, int p_j);

void Transpose(Image* I, Image* new_I);

Image* Ajout_Bordure(Image* I, int n);

int Tri_Tableau(int* tableau, int taille);

int Mediane(Image* I, int p_i, int p_j);

void Mat_GxGy(Image* I, Image* Lxy);

Pixel Somme_Convolution(int i, int j, Image* New_I, const int (*Filter)[3]);

void Convolution(Image* I, const int (*Filter)[3]);

Pixel Sobel_sqrt(Pixel P1, Pixel P2);

Pixel Sobel_abs(Pixel P1, Pixel P2);

void Sobel(Image* I,Image* Lx,Image* Ly, Pixel (*function)(Pixel, Pixel));

void Compare_Laplacien(Image* I, Image* Gxy, Image* Lxy);

void Troncature(Image* Lxy);

void Copie_Image(Image* I, Image* Copie);

#endif