/**
 * \file fonctions_gestion_image.c
 * \brief Contient les fonctions qui permettent de récupérer les images et de les enregistrer.
 * \author Thomas BOULBES, Ivan HANZEK, Romain PAPOT, Maxime TAUZIA
 * \version 0.1
 * \date 10 septembre 2015 au 20 octobre 2015
 *
 *
 *
 */
#include "fonctions_gestion_image.h"


/**
 * \fn Image* NouvelleImage(int r,int c)
 * \author Thomas BOULBES, Ivan HANZEK, Romain PAPOT, Maxime TAUZIA
 * \date 28 septembre 2015
 * \brief  Crée une image de taille r*c et la retourne.
 * \param 	r : nombre de lignes du tableau (entier)
 * \param 	c : nombre de colonnes du tableau (entier)
 * \return retourne une Image (Image est une structure, voire librairie.h)
 */
Image* NouvelleImage(int r,int c)
{
	int i;
	Image* I = malloc(sizeof(Image)); //affectation de mémoire pour l'image. 
	I->c = c;
	I->r = r;
	(I->Tab) = malloc(sizeof *(I->Tab) * r);  //affectation de mémoire pour le tableau.    
	if (I->Tab)
	{
	  for (i = 0; i < r; i++)
	  {
	    (I->Tab)[i] = malloc(sizeof *(I->Tab)[i] * c);
	  }
	}
	return I;
}


/**
 * \fn void DelImage(Image* I)
 * \author Thomas BOULBES, Ivan HANZEK, Romain PAPOT, Maxime TAUZIA
 * \date 28 septembre 2015
 * \brief  Libère l'espace mémoire occupé par l'image.
 * \param 	 I -> Image
 */
void DelImage(Image* I)
{
	if (I) {
		int i;
		for (i = 0; i < I->r; ++i) free(I->Tab[i]); //Désallocation dynamique de la mémoire du Tableau. 
		free(I);
	}
}

/**
 * \fn int Sauver(Image* I, const char* fichier)
 * \author Thomas BOULBES, Ivan HANZEK, Romain PAPOT, Maxime TAUZIA
 * \date 28 septembre 2015
 * \brief   Sauvegarde le Tableau de Pixel dans un fichier qui sera lu comme une image.
 * \param 	I -> Image
 * \param 	fichier -> Adresse et nom du fichier à créer
 * \return La fonction retourne -1 si on a pas pu créer de fichier.
 * \return La fonction retourne 0 si tout s'est bien passé.
 */
int Sauver(Image* I, const char* fichier)
{
	int i, j;
	FILE* F = fopen(fichier,"w");//On ouvre le fichier et on s'apprête à écrire dessus ("w"). 
	fprintf(F,"P3\n%d %d\n255\n", I->c, I->r);//On écrit les premières lignes du fichier. 
	for(i = 0; i < I->r; i++)//On écrit dans le fichier.... 
	{
		for (j = 0; j < I->c; j++)
		{
			fprintf(F,"%d %d %d ",I->Tab[i][j].r,I->Tab[i][j].g,I->Tab[i][j].b);//Les lignes suivantes correspondent aux pixels de chaque case du tableau.  
		}															
	}	
	fclose(F); //Fermeture du fichier. 
	return 0;
}	


/**
 * \fn void Reecriture(char* fichier)
 * \author Thomas BOULBES, Ivan HANZEK, Romain PAPOT, Maxime TAUZIA
 * \date 5 octobre 2015
 * \brief Réécris le fichier mis en paramètre dans un fichier temporaire, sans commentaire.
 * \param 	fichier -> Nom du fichier contenant les valeurs à réécrire (L'image)
 */
void Reecriture(char* fichier) {
	int ret;
	char temp;
	FILE* F = NULL;
	F = fopen(fichier,"r"); //On ouvre le fichier et on s'apprête à le lire ("r"). 
	
	FILE* F_temp = fopen("tmp.ppm", "w");//On ouvre/crée le fichier temporaire et on s'apprête à écrire dessus ("w"). 
	while ((ret = fscanf (F, "%c", &temp)) != EOF && ret != 0) 
	    { 
		if (temp == 35) { //Si le charactère récupéré est un #.... */ 
			while (temp != 10) {
				fscanf(F, "%c", &temp); //On va jusqu'à la fin de la ligne. (\n = 10 en ascii). 
			}
		} else {
			fprintf(F_temp, "%c", temp); //Sinon on écris ce que l'on a récupéré dans le fichier temporaire. 
				}
	}
	fclose(F_temp); //On ferme le fichier temporaire. 
	fclose(F);	//On ferme le fichier. 
}

/**
 * \fn Image* Pre_Crop(Image* I, Pos* debut, Pos* fin)
 * \author Thomas BOULBES, Ivan HANZEK, Romain PAPOT, Maxime TAUZIA
 * \date 28 septembre 2015
 * \brief   Le type de fichier doit avoir comme extension P3.
 * \param  fichier -> adresse et nom du fichier à ouvrir et à stocker.
 * \return Retourne le pointeur d'image dans lequel on a stocké le fichier.
 */
Image* Charger(char* fichier)
{
	int i,j,max;
	Pixel p;
	char type[10];//Premier élément contenu dans le fichier : Cf remarque.  
	Image* I;
	Reecriture(fichier);  //On réécris le fichier sans les commentaires dans un fichier temporaire. 
	FILE* F = fopen("tmp.ppm","r");  //On ouvre le fichier temporaire et on s'apprête à le lire ("r"). 
	fscanf(F,"%s %d %d %d",type,&j,&i,&max); //On récupère les premières données contenues dans le fichier temporaire. 
	I = NouvelleImage(i,j); //On crée une nouvelle image de taille i et j (récupérée au début du fichier temporaire). 
	for(i = 0; i < I->r; i++)	//et on y stocke les valeurs contenues dans le fichier temporaire.. 
	{
		for (j = 0; j < I->c; j++)
		{
		fscanf(F,"%d %d %d",&p.r,&p.g,&p.b); //on récupère les valeurs du fichier temporaire 3 par 3. */ 
		I->Tab[i][j] = p; //Puis on les stocke dans l'image.. 
		}
	}
	fclose(F); //Fermeture du fichier temporaire.. 
	remove("tmp.ppm"); //Supression du fichier temporaire. 
	return I; //Retour du pointeur de l'image. 
}