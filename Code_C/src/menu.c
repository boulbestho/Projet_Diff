/**
 * \file menu.c
 * \brief Contient l'affichage du menu, et la redirection vers les autres fonctionnalitées du programme. 
 * \author Thomas BOULBES, Ivan HANZEK, Romain PAPOT, Maxime TAUZIA
 * \version 0.1
 * \date 10 septembre 2015 au 20 octobre 2015
 *
 *
 *
 */
#include "menu.h"
#include "fonctionnalités_basiques.h"
#include "fonctionnalités_avancées.h"
#include "fonctions_annexes.h"
#include "fonctions_gestion_image.h"
#include <limits.h>
#include <time.h>
#include <unistd.h>


/**
 * \fn void Menu(int choix, char fichier[256] ) 
 * \author Thomas BOULBES, Ivan HANZEK, Romain PAPOT, Maxime TAUZIA
 * \brief Fonction qui affiche le menu et redirige vers les autres fonctionnalitées.
 */
	
void Menu(int choix,  char fichier[256], int seuil_bi, Pos debut, Pos fin) 
{	
	    char nom_fichier[256];
	    char cwd[256];
	    int nb = 0;
		Image* new_I;  
	    Image* I = Charger(fichier);
	    time_t t;

	    srand((unsigned)time(&t));
	    chdir("../Web/uploads");
	    getcwd(cwd, sizeof(cwd));
	    nb = rand() % INT_MAX;
	    sprintf(cwd, "%s/%d.ppm", cwd, nb);
	    sprintf(nom_fichier, "%d.ppm", nb);

	    switch (choix) {
			case 1 :
			Binarisation(I, seuil_bi);
			Sauver(I, cwd);
			break;

			case 2 :
			Parcourir(I,&Negatif);
			Sauver(I, cwd);
			break;

			case 3 :
			Parcourir(I,&Niveau_De_Gris);
			Sauver(I, cwd);
			break;

			case 4 :
			Contraste(I);
			Sauver(I, cwd);
			break;

			case 5 :
			Symetrie_H(I);
			Sauver(I, cwd);
			break;

			case 6 :
			Symetrie_V(I);
			Sauver(I, cwd);
			break;

			case 7 :
			new_I = Pre_Crop(I, &debut, &fin);
			Crop(I, new_I, debut, fin);
			Sauver(new_I, cwd);
			DelImage(new_I);
			break;

			case 8 :
			Applique_Filtre(1,I);
			Sauver(I, cwd);
			break;

			case 9 :
			Applique_Filtre(2,I);
			Sauver(I, cwd);
			break;

			case 10 :
			Applique_Filtre(3,I);
			Sauver(I, cwd);
			break;

			case 11 :
			Applique_Filtre(4,I);
			Sauver(I, cwd);
			break;

			case 12 :
			Contours_Sobel(I,seuil_bi);
			Sauver(I, cwd);
			break;

			case 13 :
			Contours_Laplacien(I,seuil_bi);
			Sauver(I, cwd);
			break;

			case 14 :
			Filtre_Median(I);
			Sauver(I, cwd);
			break;
		}
    	DelImage(I);
	    printf("%s", nom_fichier);
}

