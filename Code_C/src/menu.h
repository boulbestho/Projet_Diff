
/**
 * \file menu.h
 * \author Thomas BOULBES, Ivan HANZEK, Romain PAPOT, Maxime TAUZIA
 * \version 0.1
 * \date 10 septembre 2015 au 20 octobre 2015
 *
 *
 *
 */
#ifndef _MENU_H_
#define _MENU_H_

#include "librairie.h"

void Menu(int choix, char fichier[256], int seuil_bi, Pos debut, Pos fin);

#endif