/**
 * \file fonctions_annexes.c
 * \brief Contient les fonctions utilent pour le bon déroulement des autres fonctionnalitées.
 * \author Thomas BOULBES, Ivan HANZEK, Romain PAPOT, Maxime TAUZIA
 * \version 0.1
 * \date 10 septembre 2015 au 20 octobre 2015
 *
 *
 *
 */
#include "fonctions_annexes.h"
#include "fonctions_gestion_image.h"


/**
 * \fn int scan_int(int choix, char* message)
 * \author Thomas BOULBES, Ivan HANZEK, Romain PAPOT, Maxime TAUZIA
 * \date 22 septembre 2015
 * \param  choix : Le choix qu'à fait l'utilisateur
 * \param  message : le message à afficher à l'utilisateur
 * \return Retourne le choix de l'utilisateur en ayant traité les erreurs
 */
 int scan_int(int choix, char* message) {
 	char a = ' ';
 	while ( scanf("%d%c", &choix, &a) != 2 || a != '\n')
 	{
 		printf("%s", message);
 	    do {
 	        scanf("%c", &a);
 	    } while (a != '\n');
 	} 
 	return choix;
 }


/**
 * \fn Pixel Applique_Luminosite(int l)
 * \author Thomas BOULBES, Ivan HANZEK, Romain PAPOT, Maxime TAUZIA
 * \date 22 septembre 2015
 * \param  l : entier correspondant à la luminosité à appliquer sur le pixel.
 * \return retourne un pixel ayant pour valeur r = g = b = l.
 */
Pixel Applique_Luminosite(int l)
 {
	Pixel p;
	p.r = l;
	p.g = l;
	p.b = l;
	return p;
}



/**
 * \fn Pos Init_Pos() {
 * \author Thomas BOULBES, Ivan HANZEK, Romain PAPOT, Maxime TAUZIA
 * \date 29 septembre 2015
 * \return Retourne un vecteur position initialisé à 0.
 */
Pos Init_Pos() 
{
	Pos position;
	position.i = 0;
	position.j = 0;
	return position;
}


/**
 * \fn void Parcourir(Image* I, Pixel (*function)(Pixel)) 
 * \author Thomas BOULBES, Ivan HANZEK, Romain PAPOT, Maxime TAUZIA
 * \date 22 septembre 2015
 * \brief La fonction parcours l'image.
 * \param 	I -> Image
 * \param	function : fonction qui prend en paramètre un pixel et qui retourne un pixel modifié.
 */
void Parcourir(Image* I, Pixel (*function)(Pixel))
 {
	int i, j;
	for (i = 0; i < I->r; ++i)
		for (j = 0; j < I->c; ++j)
		{
			I->Tab[i][j] = function(I->Tab[i][j]); //Le pixel de la case prend la valeur du retour de la fonction appliquée.
		}
}


/**
 * \fn void Get_Max_Min(Image* I, Pixel* p_max, Pixel* p_min)
 * \author Thomas BOULBES, Ivan HANZEK, Romain PAPOT, Maxime TAUZIA
 * \date 22 septembre 2015
 * \param 	I -> Image
 * \param	p_max -> Pointeur du pixel contenant les valeurs max
 * \param	p_min -> Pointeur du pixel contenantles valeurs min
 */
void Get_Max_Min(Image* I, Pixel* p_max, Pixel* p_min) {
	int i, j;
	for (i = 0; i < I->r; ++i)
		{
		for (j = 0; j < I->c; ++j)
			{
				if (I->Tab[i][j].r >= p_max->r) p_max->r = I->Tab[i][j].r;
				if (I->Tab[i][j].g >= p_max->g) p_max->g = I->Tab[i][j].g;
				if (I->Tab[i][j].b >= p_max->b) p_max->b = I->Tab[i][j].b;
				if (I->Tab[i][j].r <= p_min->r) p_min->r = I->Tab[i][j].r;
				if (I->Tab[i][j].g <= p_min->g) p_min->g = I->Tab[i][j].g;
				if (I->Tab[i][j].b <= p_min->b) p_min->b = I->Tab[i][j].b;
			}
		}
}


/**
 * \fn int Compte_Valeurs(Image* I, int p_i, int p_j)
 * \author Thomas BOULBES, Ivan HANZEK, Romain PAPOT, Maxime TAUZIA
 * \date 10 septembre 2015
 * \param  	I -> Image à traiter
 * \param	p_i : Position en ordonnée (entier)
 * \param	p_j : Position en abscisse (entier)
 * \return Retourne le nombre de valeur autours du pixel qui ne valent pas -1.
 */
int Compte_Valeurs(Image* I, int p_i, int p_j) 
{
	int i, j;
	int nb = 0;
	for (i = p_i-1; i <= p_i+1; i++)
	{
		for (j = p_j-1; j <= p_j+1; j++)
		{
			if (I->Tab[i][j].r != -1) nb++;  //Si la valeur n'est pas -1, on augmente le résultat. 
		}
	}
	return nb;
}


/**
 * \fn void Transpose(Image* I, Image* new_I)
 * \author Thomas BOULBES, Ivan HANZEK, Romain PAPOT, Maxime TAUZIA
 * \date 10 septembre 2015
 * \brief Met les valeurs de l'image de base au centre de la nouvelle image (Qui aura donc des bordures).
 * \param 	I -> Image de base
 * \param 	new_I -> Nouvelle image
*/
void Transpose(Image* I, Image* new_I)
{
	int i,j;
	for (i = 0; i <I->r; i++)
	{
		for (j = 0; j < I->c ; j++)
		{
			new_I->Tab[i+1][j+1] = I->Tab[i][j];
		}
	}
}


/**
 * \fn Image* Ajout_Bordure(Image* I, int n) 
 * \author Thomas BOULBES, Ivan HANZEK, Romain PAPOT, Maxime TAUZIA
 * \date 10 septembre 2015
 * \param  	I -> Image sur laquelle on veut ajouter des bordures
 * \param	n : Valeur sur les bordures
 * \return Retourne une nouvelle image avec des bordures et les valeurs de l'image initiale.
 */
Image* Ajout_Bordure(Image* I, int n)
{
	int i,j;
	Image* new_I = NouvelleImage((I->r)+2,(I->c)+2);//Crée la nouvelle image ayant 2 lignes et 2 colones de plus que l'image initiale. 
	for (i = 0; i <(I->r)+2 ; i++)
	{
		for (j = 0; j < (I->c)+2; ++j)
		{
			new_I->Tab[i][j] = Applique_Luminosite(n);//Initialise la nouvelle image avec la valeur n. 
		}
	}
	Transpose(I, new_I);//Met les valeurs de l'image au centre de la nouvelle image.  
	return new_I;
}




/**
 * \fn int Tri_Tableau(int* tableau, int taille)
 * \author Thomas BOULBES, Ivan HANZEK, Romain PAPOT, Maxime TAUZIA
 * \date 10 septembre 2015
 * \brief   Le type de fichier doit avoir comme extension P3.
 * \param  	tableau : tableau d'entiers à trier 
 * \param	taille -> taille du tableau (entier)
 * \return Retourne la médiane du tableau une fois celui-ci trié.
 */
int Tri_Tableau(int* tableau, int taille) 
{
	int i, j, a;
	for (i = 0; i < taille; ++i)
	    {
	        for (j = i + 1; j < taille; ++j)
	        {
	            if (tableau[i] > tableau[j])
	            {
	                a =  tableau[i];
	                tableau[i] = tableau[j];
	                tableau[j] = a;
	            }
	        }
	    }
	return (tableau[taille/2]);
}


/**
 * \fn int Mediane(Image* I, int p_i, int p_j)
 * \author Thomas BOULBES, Ivan HANZEK, Romain PAPOT, Maxime TAUZIA
 * \date 10 septembre 2015
 * \brief   Le type de fichier doit avoir comme extension P3.
 * \param 	I -> Image
 * \param	p_i : Position en ordonnée
 * \param	p_j : Position en abscisse
 * \return  Retourne la médiane des valeurs autours d'un pixel.
 */
int Mediane(Image* I, int p_i, int p_j) 
{
	int i, j;
	int k = 0;
	int nb = Compte_Valeurs(I, p_i, p_j);
	int* tableau = malloc(nb*sizeof(int));
	for (i = p_i-1; i <= p_i+1; i++)
	{
		for (j = p_j-1; j <= p_j+1; j++)
		{
			if (I->Tab[i][j].r != -1) 
			{
				tableau[k] = I->Tab[i][j].r; 
				k++;
			}
		}
	}
	int Mediane = Tri_Tableau(tableau, nb);
	free(tableau);
	return Mediane;
}

/**
 * \fn void Mat_GxGy(Image* I, Image* Lxy)
 * \author Thomas BOULBES, Ivan HANZEK, Romain PAPOT, Maxime TAUZIA
 * \date 7 octobre 2015
 * \brief Modifie Lxy.
 * \param 	I -> Image de base
 * \param	Lxy -> Image de sortie contenant les valeurs de G(x,y)
 */
void Mat_GxGy(Image* I, Image* Lxy) {
	int i, j;
	Image* Lx = NouvelleImage(I->r, I->c); 
	Image* Ly = NouvelleImage(I->r, I->c);
	Copie_Image(I, Lxy); //Lxy devient une copie de I.  
	Convolution(I, Filtre_4);//On applique le filtre 4 à I.  
	Copie_Image(I, Lx); //Lx devient une copie de I
	Convolution(Lxy, Filtre_5);//On applique le filtre 5 à Lxy (qui possède les valeur du I de base). 
	Copie_Image(Lxy, Ly);//Ly devient une copie de Lxy. */   
	for (i = 0; i < I->r; ++i)
	{
		for (j = 0; j < I->c; ++j)
		{
			Lxy->Tab[i][j] = Applique_Luminosite(Lx->Tab[i][j].r + Ly->Tab[i][j].r); //Ly devient une copie de Lxy. 
		}
	}
	DelImage(Lx); //On supprime Lx. 
	DelImage(Ly);//On supprime Ly. 
}


/**
 * \fn Pixel Somme_Convolution(int i , int j, Image* new_I, const int (*Filter)[3])
 * \author Thomas BOULBES, Ivan HANZEK, Romain PAPOT, Maxime TAUZIA
 * \date 1 octobre 2015
 * \brief   applique le filtre sur le pixel en question et retourne la somme pour chaques couleurs dans le pixel.
 * \param   i , j : coordonner de la case de debut (entier)
 * \param	new_I -> image réference entouré de 0 pour que la convolution se passe bien
 * \param	Filter : filtre 3*3 a appliquer sur new_I
 * \return Retourne un Pixel
 */
Pixel Somme_Convolution(int i , int j, Image* new_I, const int (*Filter)[3])
{ 
	int x,y;
	Pixel somme;
	somme=Applique_Luminosite(0);
	for (x = 0; x < 3 ; x++)
	{
		for (y=0 ; y < 3 ; y++)
		{	
				somme.r=somme.r+(new_I->Tab[i+x][j+y].r)*Filter[x][y];
				somme.g=somme.g+(new_I->Tab[i+x][j+y].g)*Filter[x][y];
				somme.b=somme.b+(new_I->Tab[i+x][j+y].b)*Filter[x][y];
		}
	}
	return somme;
}

/**
 * \fn void Convolution(Image* I, const int (*Filter)[3])
 * \author Thomas BOULBES, Ivan HANZEK, Romain PAPOT, Maxime TAUZIA
 * \date 1 octobre 2015
 * \brief La fonction applique une covolution sur chaques pixels de l'image grace à l'appelle d'une autre fonction nommée Somme_convolution.
 * \param 	I -> Image
 * \param	int (*filter)[3] : tableau 3*3
 */
void Convolution(Image* I, const int (*Filter)[3])
{	
	int i,j;
	Pixel somme;
	Image* new_I = Ajout_Bordure(I, 0);
	for (i = 0; i < I->r;i++)
	{
		for (j = 0; j < I->c ; j++)
		{				
			somme = Somme_Convolution(i, j, new_I, Filter);
			I->Tab[i][j].r=somme.r;
			I->Tab[i][j].g=somme.g;
			I->Tab[i][j].b=somme.b;
		}
	}
	DelImage(new_I);
}


/**
 * \fn Pixel Sobel_sqrt(Pixel P1, Pixel P2)
 * \author Thomas BOULBES, Ivan HANZEK, Romain PAPOT, Maxime TAUZIA
 * \date 1 octobre 2015
 * \brief  La fonction Sobel_sqrt prend deux pixels en entré pour en resortir un unique grâce à une première méthode qui utilise la racine ².
 * \param  P1 : Pixel
 * \param  P2 : pixel

 * \return Retourne un pixel.
 */
Pixel Sobel_sqrt(Pixel P1, Pixel P2)
{
	Pixel Pf;
	Pf.r = sqrt(P1.r*P1.r + P2.r*P2.r);
	Pf.g = sqrt(P1.g*P1.g + P2.g*P2.g);
	Pf.b = sqrt(P1.b*P1.b + P2.b*P2.b);
	return Pf;
}


/**
 * \fn Pixel Sobel_abs(Pixel P1, Pixel P2)
 * \author Thomas BOULBES, Ivan HANZEK, Romain PAPOT, Maxime TAUZIA
 * \date 1 octobre 2015
 * \brief  La fonction Sobel_sqrt prend deux pixels en entré pour en resortir un unique grâce à une première méthode qui utilise la valeur absolue.
 * \param  P1 : Pixel
 * \param  P2 : pixel
 * \return Retourne un pixel.
 */
Pixel Sobel_abs(Pixel P1, Pixel P2)
{
	Pixel Pf;
	Pf.r = abs(P1.r)+abs(P2.r);
	Pf.g = abs(P1.g)+abs(P2.g);
	Pf.b = abs(P1.b)+abs(P2.b);
	return Pf;
}

/**
 * \fn void Sobel(Image* I,Image* Lx,Image* Ly, Pixel (*function)(Pixel, Pixel)) 
 * \author Thomas BOULBES, Ivan HANZEK, Romain PAPOT, Maxime TAUZIA
 * \date 1 octobre 2015
 * \brief La fonction sobel va renvoyer une nouvelle image grâce à l'operateur de Sobel qui va être appliqué sur chaque Pixel.
 * \param 	I- > Image
 * \param	Lx -> Image
 * \param	Ly -> Image
 * \param	Pixel -> fonction qui retourne un Pixel
 */
void Sobel(Image* I,Image* Lx,Image* Ly, Pixel (*function)(Pixel, Pixel)) 
{
	int i, j;
	for (i = 0; i < I->r; ++i)
	{
		for (j = 0; j < I->c; ++j)
		{
			I->Tab[i][j] = function(Lx->Tab[i][j],Ly->Tab[i][j]);
		}
	}
}


/**
 * \fn void Compare_Laplacien(Image* I, Image* Gxy, Image* Lxy)
 * \author Thomas BOULBES, Ivan HANZEK, Romain PAPOT, Maxime TAUZIA
 * \date 8 octobre 2015
 * \brief Modifie I.
 * \param 	I -> Image à traiter
 * \param	Gxy -> Image I à laquelle on a appliqué le gradient simple
 * \param	Lxy -> Image I à laquelle on a appliqué le laplacien
 */
void Compare_Laplacien(Image* I, Image* Gxy, Image* Lxy)
{
	int i, j;
	int G0 = 15;
	int L0 = 250;
	
	for (i = 0; i < I->r; ++i)
	{
		for (j = 0; j < I->c; ++j)
		{
			if (abs(Gxy->Tab[i][j].r) < G0)
			{
				I->Tab[i][j] = Applique_Luminosite(0);
			}

			if (abs(Lxy->Tab[i][j].r) > L0)
			{
				I->Tab[i][j] = Applique_Luminosite(255);
			}
		}
	} 
}

/**
 * \fn void Troncature(Image* Lxy)
 * \author Thomas BOULBES, Ivan HANZEK, Romain PAPOT, Maxime TAUZIA
 * \date 7 octobre 2015
 * \brief Toutes les valeurs au dessus de 255 sont tronquées à 255.
 * \param Lxy -> Image contenant les valeurs de lx + Ly
 */
void Troncature(Image* Lxy) 
{
	int i, j;
	for (i = 0; i < Lxy->r; ++i)
	{
		for (j = 0; j < Lxy->c; ++j)
		{
			if (Lxy->Tab[i][j].r > 255) Lxy->Tab[i][j] = Applique_Luminosite(255);
		}
	}
}


/**
 * \fn void Copie_Image(Image* I, Image* Copie)
 * \author Thomas BOULBES, Ivan HANZEK, Romain PAPOT, Maxime TAUZIA
 * \date 7 octobre 2015
 * \brief Copie possède les valeurs de I
 * \param  	I -> Image à copier
 * \param	Copie -> Copie de l'image
 */
void Copie_Image(Image* I, Image* Copie) 
{
	int i, j;
	Copie->r = I->r;
	Copie->c = I->c;
	for (i = 0; i < I->r; ++i)
	{
		for (j = 0; j < I->c; ++j)
		{
			Copie->Tab[i][j].r = I->Tab[i][j].r;
			Copie->Tab[i][j].g = I->Tab[i][j].g;
			Copie->Tab[i][j].b = I->Tab[i][j].b;
		}
	}
}
