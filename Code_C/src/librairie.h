/**
 * \file librairie.h
 * \author Thomas BOULBES, Ivan HANZEK, Romain PAPOT, Maxime TAUZIA
 * \version 0.1
 * \date 10 septembre 2015 au 20 octobre 2015
 *
 *
 *
 */
#ifndef _LIBRAIRIE_H_
#define _LIBRAIRIE_H_

#include <stdio.h>
#include <stdlib.h>
#include <assert.h>
#include <string.h>
#include <math.h>

static const char clear[8] = "\033[2J";

static const int Filtre_1[3][3] = {{1,1,1},{1,1,1},{1,1,1}};
static const int Filtre_2[3][3] = {{-1,1,0},{0,0,0},{0,0,0}};
static const int Filtre_3[3][3] = {{-1,0,0},{1,0,0},{0,0,0}};
static const int Filtre_4[3][3] = {{-1,0,1},{-2,0,2},{-1,0,1}};
static const int Filtre_5[3][3] = {{1,2,1},{0,0,0},{-1,-2,-1}};
static const int Filtre_6[3][3] = {{0,1,0},{1,-4,1},{0,1,0}};


/**
 * \struct _Pos
 * \brief Position des pixels
 */
typedef struct _Pos
{
	int i;
	int j;
} Pos;


/**
 * \struct _Pixel
 * \brief structure Pixel
 *
 * La structure Pixel est composée de 3 integers representant ses 3 couleurs fondamentales (rouge,vert,bleau).
 *
 */
typedef struct _Pixel
{
	int r; 
	int g; 
	int b; 
} Pixel;


/**
 * \struct _Image
 * \brief Structure Image
 *
 * La structure Image contient ses dimensions (r:rows (Lignes) et c:columns(Colonnes)).
 * mais aussi un pointeur de pointeur de Pixel qui va servir à créer l'image.
 *
 */
typedef struct _Image
{
	int r; 
	int c; 
	Pixel **Tab;
} Image;

#endif