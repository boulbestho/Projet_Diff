var searchData=
[
  ['main',['main',['../main_8c.html#ae66f6b31b5ad750f1fe042a706a4e3d4',1,'main.c']]],
  ['main_2ec',['main.c',['../main_8c.html',1,'']]],
  ['mat_5fgxgy',['Mat_GxGy',['../fonctions__annexes_8c.html#a0067816dcfc5ed5d495fff6da7e05820',1,'Mat_GxGy(Image *I, Image *Lxy):&#160;fonctions_annexes.c'],['../fonctions__annexes_8h.html#a0067816dcfc5ed5d495fff6da7e05820',1,'Mat_GxGy(Image *I, Image *Lxy):&#160;fonctions_annexes.c']]],
  ['mediane',['Mediane',['../fonctions__annexes_8c.html#a6d7cb6f26f71771e01ec334ae231ebfd',1,'Mediane(Image *I, int p_i, int p_j):&#160;fonctions_annexes.c'],['../fonctions__annexes_8h.html#a6d7cb6f26f71771e01ec334ae231ebfd',1,'Mediane(Image *I, int p_i, int p_j):&#160;fonctions_annexes.c']]],
  ['menu',['Menu',['../menu_8c.html#afdf1ca9e7afc3e7ec41b47fea4b3d80d',1,'Menu():&#160;menu.c'],['../menu_8h.html#afdf1ca9e7afc3e7ec41b47fea4b3d80d',1,'Menu():&#160;menu.c']]],
  ['menu_2ec',['menu.c',['../menu_8c.html',1,'']]],
  ['menu_2eh',['menu.h',['../menu_8h.html',1,'']]]
];
